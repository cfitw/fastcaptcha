package com;

import com.captcha.GifCaptcha;
import com.captcha.PngCaptcha;
import com.captcha.config.Config;
import com.captcha.enums.CaptchaConstant;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Properties;



/**
 * @Author: 随风飘的云
 * @Description: 注意验证码图片输出位置
 * @Date: 2022/3/4 18:36
 * @Modified By:
 */

public class CaptchaTest{
    @Test
    public void getPngCaptcha() throws Exception{
        PngCaptcha captcha = new PngCaptcha();
        Properties properties = new Properties();
        // properties.setProperty(CaptchaConstant.CAPTCHA_BORDER, "no");
        properties.setProperty(CaptchaConstant.CAPTCHA_BORDER_THICKNESS, "5");
        properties.setProperty(CaptchaConstant.CAPTCHA_BORDER_COLOR, "0,0,0");
        Config config = new Config(properties);
        captcha.setConfig(config);
        String text = captcha.getText();
        System.out.println(text);
        BufferedImage image = captcha.RenderImage(text);
        captcha.write(new FileOutputStream(new File("F:\\test\\aa\\1.png")), image);
    }

    @Test
    public void getGifCaptcha() throws Exception {
        Properties properties = new Properties();
        GifCaptcha captcha = new GifCaptcha();
        properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_TYPE, "ARITHMETIC_ZH");
        Config config = new Config(properties);
        captcha.setConfig(config);
        String text = captcha.getText();
        System.out.println(text);
        int temp = text.indexOf("?");
        captcha.createGif(text.substring(0, temp));
        captcha.write(new FileOutputStream(new File("F:\\test\\aa\\2.gif")), captcha.getImageBytes());
    }

}
