package com.captcha.config;

import java.io.Serializable;

/**
 * @Author: 随风飘的云
 * @Description: 设置配置类的获取方法
 * @Date: 2022/3/4 18:46
 * @Modified By:
 */
public abstract class Configurable implements Serializable {

    private Config config = null;

    public void setConfig(Config config) {
        this.config = config;
    }
    public Config getConfig(){
        return this.config;
    }
}
