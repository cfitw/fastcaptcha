package com.captcha.config;

import com.captcha.enums.CaptchaConstant;
import com.captcha.enums.CaptchaType;
import com.captcha.enums.TextType;
import com.captcha.exception.ConfigException;
import com.captcha.utils.FontArrays;

import java.awt.*;
import java.util.Properties;

/**
 * @Author: 随风飘的云
 * @Description: 设置配置类
 * @Date: 2022/3/4 18:45
 * @Modified By:
 */
public class Config {

    private Properties properties;

    private ConfigHelper helper;

    // 有参构造方法
    public Config(Properties properties){
        this.properties = properties;
        this.helper = new ConfigHelper();
    }

    public Properties getProperties(){
        return this.properties;
    }

    /**
     * 配置内容格式
     * @return
     */
    public String getContentType(){
        String paramName = CaptchaConstant.CAPTCHA_TEXT_TYPE;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setType(paramName, paramValue, "UPPER_STRING", TextType.contentType());
    }

    /**
     * 配置图片格式
     * @return
     */
    public String getImageType(){
        String paramName = CaptchaConstant.CAPTCHA_IMAGE_TYPE;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setType(paramName, paramValue, "png", CaptchaType.getCaptchaType());
    }

    /**
     * 设置前端的session的key
     * @return
     */
    public String getSessionKey(){
        return this.properties.getProperty(CaptchaConstant.CAPTCHA_SESSION_KEY, CaptchaConstant.CAPTCHA_CONFIG_KEY);
    }

    /**
     * 设置前端的date
     * @return
     */
    public String getSessionDate(){
        return this.properties.getProperty(CaptchaConstant.CAPTCHA_SESSION_DATE, CaptchaConstant.CAPTCHA_CONFIG_DATE);
    }

    /**
     * 设置是否需要边框，未测试
     * @return
     */
    public boolean isBorder(){
        String paramName = CaptchaConstant.CAPTCHA_BORDER;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setBoolean(paramName, paramValue, true);
    }

    /**
     * 设置边框的颜色，未测试
     * @return
     */
    public Color setBorderColor(){
        String paramName = CaptchaConstant.CAPTCHA_BORDER_COLOR;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.getColor(paramName, paramValue, Color.GRAY);
    }

    /**
     * 设置边框厚度
     * @return
     */
    public int getThickness(){
        String paramName = CaptchaConstant.CAPTCHA_BORDER_THICKNESS;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setPositiveInt(paramName, paramValue, 1);
    }

    /**
     * 设置干扰圆
     * @return
     */
    public int getNoiseNumber(){
        String paramName = CaptchaConstant.CAPTCHA_NOISE_NUMBER;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setPositiveInt(paramName, paramValue, 2);
    }

    /**
     * 设置干扰线
     * @return
     */
    public int getShadeNumber(){
        String paramName = CaptchaConstant.CAPTCHA_SHADE_NUMBER;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setPositiveInt(paramName, paramValue, 1);
    }

    /**
     * 设置干扰线厚度
     * @return
     */
    public int getShadeThickness(){
        String paramName = CaptchaConstant.CAPTCHA_SHADE_THICKNESS;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setPositiveInt(paramName, paramValue, 1);
    }

    /**
     * 添加扭曲效果
     * @return
     */
    public boolean addShear(){
        String paramName = CaptchaConstant.CAPTCHA_IMAGE_SHEAR_ADD;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setBoolean(paramName, paramValue, true);
    }

    /**
     * 设置内容长度，默认为4
     * @return
     */
    public int getTextLength(){
        // 获取长度
        String paramName = CaptchaConstant.CAPTCHA_TEXT_LENGTH;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setPositiveInt(paramName, paramValue, 4);
    }

    public int getTextThickness(){
        String paramName = CaptchaConstant.CAPTCHA_TEXT_FONT_THICKNESS;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setPositiveInt(paramName, paramValue, 2);
    }

    /**
     * 设置图片宽度
     * @return
     */
    public int getWidth(){
        String paramName = CaptchaConstant.CAPTCHA_IMAGE_WIDTH;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setPositiveInt(paramName, paramValue, 200);
    }

    /**
     * 设置图片高度
     * @return
     */
    public int getHeight(){
        String paramName = CaptchaConstant.CAPTCHA_IMAGE_HEIGHT;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setPositiveInt(paramName, paramValue, 50);
    }

    // 设置内部系统字体（支持多个字体设置）
    public Font[] getTextFontType(int fontSize){
        String paramName = CaptchaConstant. CAPTCHA_TEXT_FONT_NAMES;
        String paramValue = this.properties.getProperty(paramName);
        // 获取字体加载类
        FontArrays fonts = new FontArrays(getContentType(),getTextFontSize());

        return this.helper.getTextFont(paramName, paramValue, fontSize, fonts.getFonts());
    }

    /**
     * 设置内容字体大小
     * @return
     */
    public int getTextFontSize(){
        String paramName = CaptchaConstant.CAPTCHA_TEXT_FONT_SIZE;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.setPositiveInt(paramName, paramValue, 30);
    }

    // 设置字体颜色（验证码背景为白色不起作用）
    public Color getTextFontColor(){
        String paramName = CaptchaConstant.CAPTCHA_TEXT_FONT_COLOR;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.getColor(paramName, paramValue, Color.WHITE);
    }

    /**
     * 设置背景颜色
     * @return
     */
    public Color getBackgroundColor(){
        String paramName = CaptchaConstant.CAPTCHA_BACKGROUND_COLOR;
        String paramValue = this.properties.getProperty(paramName);
        return this.helper.getColor(paramName, paramValue, Color.WHITE);
    }

}
