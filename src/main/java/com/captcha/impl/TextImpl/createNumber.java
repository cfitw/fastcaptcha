package com.captcha.impl.TextImpl;

import com.captcha.config.Configurable;
import com.captcha.impl.TextService;
import com.captcha.utils.RandomUtils;

/**
 * @Author: 随风飘的云
 * @Description: 数字验证码生成器
 * @Date: 2022/3/5 14:27
 * @Modified By:
 */
public class createNumber implements TextService {

    @Override
    public String getContent(String type, int length) {
        if(type.equals("NUMBER")){
            return RandomUtils.RandomNumber(length);
        }else if(type.equals("NUMBER_ZH")){
            return RandomUtils.RandomComplexNumber(length);
        }
        return null;
    }
}
