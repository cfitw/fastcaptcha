package com.captcha.impl.TextImpl;

import com.captcha.config.Configurable;
import com.captcha.impl.TextService;
import com.captcha.utils.RandomUtils;

/**
 * @Author: 随风飘的云
 * @Description: 中文验证码生成器
 * @Date: 2022/3/5 14:53
 * @Modified By:
 */
public class createChinese implements TextService {

    @Override
    public String getContent(String type, int length) {
        if(type.equals("CHINESE")){
            return RandomUtils.randomChinese(length);
        }else if(type.equals("CHINESE_IDIOM")){
            String str = RandomUtils.randomIdiom();
            int[] num = RandomUtils.getRandomIdiom();
            StringBuffer buffer = new StringBuffer();
            buffer.append(str)
                    .append(",");
            for (int i = 0; i < 4; i++) {
                buffer.append(str.charAt(num[i]));
            }
            return new String(buffer);
        }
        return null;
    }
}
