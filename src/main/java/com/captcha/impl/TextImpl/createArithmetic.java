package com.captcha.impl.TextImpl;

import com.captcha.enums.Constants;
import com.captcha.impl.TextService;
import com.captcha.utils.RandomUtils;

/**
 * @Author: 随风飘的云
 * @Description: 算术验证码内容生成器
 * @Date: 2022/3/5 14:58
 * @Modified By:
 */
public class createArithmetic implements TextService {

    /**
     * 拼接字符串
     * @param operation 运算符
     * @param x
     * @param y
     * @param str1
     * @param str2
     * @return
     */
    private String Splicing(int operation, int x, int y, String str1, String str2, int type){
        Integer result = 0;
        StringBuilder builder = new StringBuilder();
        if(operation == 0){
            result = x * y;
            builder.append(str1);
            if(type == 0){
                builder.append("*");
            }else {
                builder.append("乘");
            }
            builder.append(str2);
        }else if(operation == 1){
            if(x!=0 && y % x == 0){
                result = y / x;
                builder.append(str2);
                if(type == 0){
                    builder.append("/");
                }else {
                    builder.append("除");
                }
                builder.append(str1);
            }else {
                result = x + y;
                builder.append(str1);
                if(type == 0){
                    builder.append("+");
                }else {
                    builder.append("加");
                }
                builder.append(str2);
            }
        }else if(operation == 2){
            if(x >= y){
                result = x - y;
                builder.append(str1);
                if(type == 0){
                    builder.append("-");
                }else {
                    builder.append("减");
                }
                builder.append(str2);
            }else {
                result = y - x;
                builder.append(str2);
                if(type == 0){
                    builder.append("-");
                }else {
                    builder.append("减");
                }
                builder.append(str1);
            }
        }else {
            result = x + y;
            builder.append(str1);
            if(type == 0){
                builder.append("+");
            }else {
                builder.append("加");
            }
            builder.append(str2);
        }
        builder.append("=?@" + result);
        return new String(builder);
    }

    /**
     * 获取阿拉伯数字验证码
     * @param length
     * @return
     */
    private String getARITHMETIC(int length){
        int len1 = RandomUtils.randomInt(1, length/2+1);
        int len2 = length - len1;
        String str1 = RandomUtils.RandomNumber(len1);
        String str2 = RandomUtils.RandomNumber(len2);
        int x = Integer.valueOf(str1);
        int y = Integer.valueOf(str2);
        int operation = (int)Math.round(RandomUtils.randomDouble() * 2);
        return Splicing(operation, x, y, str1, str2, 0);
    }

    /**
     * 获取中文简体数字验证码
     * @param length
     * @return
     */
    private String getARITHMETIC_ZH(int length){
        int len1 = RandomUtils.randomInt(1, length/2+1);
        int len2 = length - len1;
        int x = 0, y = 0;
        // 获取运算符
        int operation = (int)Math.round(RandomUtils.randomDouble() * 2);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < len1; i++) {
            int temp = RandomUtils.randomInt(10);
            if(temp == 0 && i == 0 && len1 > 1){
                continue;
            }
            x = x * 10 + temp;
            builder.append(Constants.BASE_COMPLEX_NUMBER[temp]);
        }
        String str1 = new String(builder);
        // 清空内容
        builder.delete(0,len1);
        for (int i = 0; i < len2; i++) {
            int temp = RandomUtils.randomInt(10);
            if(temp == 0 && i == 0 && len2 > 1){
                continue;
            }
            y = y * 10 + temp;
            builder.append(Constants.BASE_COMPLEX_NUMBER[temp]);
        }
        String str2 = new String(builder);
        return Splicing(operation, x, y, str1, str2, 1);
    }

    @Override
    public String getContent(String type, int length) {
        if(type.equals("ARITHMETIC")){
            return getARITHMETIC(length);
        }else if(type.equals("ARITHMETIC_ZH")){
            return getARITHMETIC_ZH(length);
        }
        return null;
    }
}
