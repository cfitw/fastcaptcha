package com.captcha.impl.TextImpl;

import com.captcha.config.Configurable;
import com.captcha.impl.TextService;
import com.captcha.utils.RandomUtils;

/**
 * @Author: 随风飘的云
 * @Description: 混合验证码生成器
 * @Date: 2022/3/5 14:57
 * @Modified By:
 */
public class createString implements TextService {

    @Override
    public String getContent(String type, int length) {
        if(type.equals("STRING")){
            return RandomUtils.RandomString(length);
        }else if(type.equals("UPPER_STRING")) {
            return RandomUtils.RandomStringUpper(length);
        }
        return null;
    }
}
