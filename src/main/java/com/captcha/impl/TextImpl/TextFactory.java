package com.captcha.impl.TextImpl;

import com.captcha.enums.TextType;
import com.captcha.exception.TextException;
import com.captcha.impl.TextService;

import java.util.*;

/**
 * @Author: 随风飘的云
 * @Description: 文本生成器工厂
 * @Date: 2022/3/5 15:32
 * @Modified By:
 */
public class TextFactory {

    private Map<String, TextService> map= new HashMap<>();

    public TextFactory(){

        List<TextService> list = new ArrayList<>();
        list.add(new createNumber());
        list.add(new createChinese());
        list.add(new createChar());
        list.add(new createString());
        list.add(new createArithmetic());
        String[] str = TextType.contentType().split(",");
        int num = 0;
        for (TextService service: list) {
            map.put(str[num++], service);
            map.put(str[num++], service);
        }

//        // 感觉还可以优化
//        map.put(TextType.NUMBER, new createNumber());
//        map.put(TextType.NUMBER_ZH, new createNumber());
//        map.put(TextType.CHAR, new createChar());
//        map.put(TextType.UPPER_CHAR, new createChar());
//        map.put(TextType.CHINESE, new createChinese());
//        map.put(TextType.CHINESE_CHICK, new createChinese());
//        map.put(TextType.STRING, new createString());
//        map.put(TextType.UPPER_STRING, new createString());
//        map.put(TextType.ARITHMETIC, new createArithmetic());
//        map.put(TextType.ARITHMETIC_ZH, new createArithmetic());
    }

    /**
     * 创建静态内部单类
     */
    public static class Holder{
        public static TextFactory instance = new TextFactory();
    }

    /**
     * 构造方法
     * @return
     */
    public static TextFactory getInstance(){
        return Holder.instance;
    }

    /**
     * 返回对象引用
     * @param type
     * @return
     */
    public TextService getCaptchaContent(String type){
        return map.get(type);
    }

//    /**
//     * 将String类型的变量转换为Enum类型的枚举对象
//     * @param tClass
//     * @param name
//     * @param <T>
//     * @return
//     */
//    private <T extends Enum<T>> T getCaptchaType(Class<T> tClass, String name){
//        if(tClass != null && name != null){
//            try {
//                return Enum.valueOf(tClass, name.toUpperCase());
//            }catch (Exception e){
//                throw new TextException("Failed to get the content format of the verification code, please check the input content format");
//            }
//        }
//        return null;
//    }
}
