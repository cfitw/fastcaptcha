package com.captcha.impl.TextImpl;

import com.captcha.config.Configurable;
import com.captcha.impl.TextService;
import com.captcha.utils.RandomUtils;

/**
 * @Author: 随风飘的云
 * @Description: 字符验证码生成器
 * @Date: 2022/3/5 14:57
 * @Modified By:
 */
public class createChar implements TextService {

    @Override
    public String getContent(String type, int length) {
        if(type.equals("CHAR")){
            return RandomUtils.RandomChar(length);
        }else if(type.equals("UPPER_CHAR")){
            return RandomUtils.RandomUpperChar(length);
        }
        return null;
    }
}
