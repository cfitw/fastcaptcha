package com.captcha.impl;

/**
 * @Author: 随风飘的云
 * @Description: 验证码生成器方法
 * @Date: 2022/3/5 0:21
 * @Modified By:
 */
public interface TextService {
    // 获取内容
    public  String getContent(String type, int length);
}
