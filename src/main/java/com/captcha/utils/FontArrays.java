package com.captcha.utils;

import com.captcha.config.Configurable;
import com.captcha.enums.CaptchaConstant;
import com.captcha.exception.TextException;
import com.captcha.impl.TextImpl.TextFactory;

import java.awt.*;

/**
 * @Author: 随风飘的云
 * @Description: 字体数组
 * @Date: 2022/3/11 10:25
 * @Modified By:
 */
public class FontArrays{

    private Font[] fonts;

    /**
     *  判断是否使用中文字体，设置字体格式需要满足有中文字体映射，
     *  简单来说，一般的字体库可以影响到输出的英文，但是映射不了中文，所以会出现乱码，
     *  如果输出的图片中出现乱码，一个原因是字体没有加载成功，一个原因是不支持用户想要的字符映射，所以出现乱码
     *  备注： 如果发现生成的验证码出现大小写错误，比如说大小的字母转成小写的，或者是小写的转成大写的，请检查加载的字体
     *  是否支持大小写映射，如果加载的字体大小写一致，那么原本大写的字母可能被转换成小写，或者是小写字母转换成大写的
     *  建议： 如果要加载字体，首先确定它是否可以正常使用，其次确认其可以支持大小写映射，或者是支持中文映射。
     *
     * @param contentType
     * @param fontSize
     */
    public FontArrays(String contentType, int fontSize){
        try {
           // 设置中文字体映射（windows10系统内置的字体）
            if(contentType.equals("NUMBER_ZH")||contentType.equals("CHINESE")
                    ||contentType.equals("CHINESE_IDIOM")||contentType.equals("ARITHMETIC_ZH")){
                fonts = new Font[]{
                        new Font("SimSun", Font.BOLD, fontSize),
                        new Font("FangSong", Font.BOLD, fontSize),
                        new Font("KaiTi", Font.BOLD, fontSize),
                        new Font("PMingLiU", Font.BOLD, fontSize)
                };
            }else {
                // 设置英文字体映射（windows10系统内置的字体）
                fonts = new Font[]{
                        new Font("Arial", Font.BOLD, fontSize),
                        new Font("Courier", Font.BOLD, fontSize),
                        new Font("Kristen ITC", Font.BOLD, fontSize),
                        new Font("Bradley Hand ITC", Font.BOLD, fontSize),
                };
            }
        } catch (Exception e){
            throw new TextException("Font load failed");
        }
    }

    public Font[] getFonts() {
        return fonts;
    }

}
