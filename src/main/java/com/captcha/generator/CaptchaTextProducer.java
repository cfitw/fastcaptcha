package com.captcha.generator;

import com.captcha.exception.TextException;
import com.captcha.impl.TextImpl.TextFactory;
import com.captcha.impl.TextService;


/**
 * @Author: 随风飘的云
 * @Description: 获取验证码内容文本，通过配置的信息进行转换
 * @Date: 2022/3/6 1:07
 * @Modified By:
 */
public class CaptchaTextProducer{

    public String getCaptchaText(String type, int length){
        TextService service = TextFactory.getInstance().getCaptchaContent(type);
        if(service == null){
            throw new TextException("There was a problem with the captcha generator factory, please re-enter the configuration requirements.");
        }
        return service.getContent(type, length);
    }

}
