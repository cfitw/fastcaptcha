package com.captcha.enums;

/**
 * @Author: 随风飘的云
 * @Description:
 * @Date: 2022/3/7 16:57
 * @Modified By:
 */
public enum  CaptchaType {

    // jpg类型的验证码
    JPG(11, "jpg"),

    // png类型的验证码
    PNG(12, "png"),

    // gif类型的验证码
    GIF(13, "gif");

    int code;
    String desc;

    CaptchaType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getCaptchaType(){
        StringBuilder builder = new StringBuilder();
        for (CaptchaType type: CaptchaType.values()) {
            builder.append(type);
            builder.append(",");
        }
        return builder.toString();
    }
}
