package com.captcha.enums;

/**
 * @Author: 随风飘的云
 * @Description: 验证码内容类型和图片类型
 * @Date: 2022/2/26 16:03
 * @Modified By:
 */
public enum TextType {

    // 数字
    NUMBER(1, "NUMBER"),

    // 中文数字
    NUMBER_ZH(2, "NUMBER_ZH"),

    // 中文
    CHINESE(3, "CHINESE"),

    // 点击中文成语
    CHINESE_IDIOM(4, "CHINESE_IDIOM"),

    // 小写英文
    CHAR(5, "CHAR"),

    // 大写
    UPPER_CHAR(6, "UPPER_CHAR"),

    // 英文和中文混合
    STRING(7, "STRING"),

    //数字和大小写字符验证码
    UPPER_STRING(8, "UPPER_STRING"),

    // 算术
    ARITHMETIC(9, "ARITHMETIC"),

    // 中文算术
    ARITHMETIC_ZH(10, "ARITHMETIC_ZH");

    int code;
    String desc;

    TextType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String contentType(){
        StringBuffer buffer = new StringBuffer();
        for (TextType type: TextType.values()) {
            buffer.append(type);
            buffer.append(",");
        }
        return new String(buffer);
    }

}
