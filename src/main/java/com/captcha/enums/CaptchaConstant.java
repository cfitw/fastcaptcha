package com.captcha.enums;

/**
 * @Author: 随风飘的云
 * @Description: 图片验证码的基本常量
 * @Date: 2022/2/23 11:50
 * @Modified By:
 */
public class CaptchaConstant {

    // session的key值
    public static final String CAPTCHA_SESSION_KEY = "captcha.session.key";

    // session的日期
    public static final String CAPTCHA_SESSION_DATE = "captcha.session.date";

    // 配置的key，传送到前端是以key-value的方式获取的
    public static final String CAPTCHA_CONFIG_KEY = "captcha.config.key";

    // 创建的时间在
    public static final String CAPTCHA_CONFIG_DATE = "captcha.config.date";

    // 边框
    public static final String CAPTCHA_BORDER = "captcha.border";

    // 边框颜色
    public static final String CAPTCHA_BORDER_COLOR = "captcha.border.color";

    // 边框厚度
    public static final String CAPTCHA_BORDER_THICKNESS = "captcha.border.thickness";

    // 设置干扰圆的数量
    public static final String CAPTCHA_NOISE_NUMBER = "captcha.noise.number";

    // 设置干扰线的数量
    public static final String CAPTCHA_SHADE_NUMBER = "captcha.shade.number";

    // 设置干扰线厚度
    public static final String CAPTCHA_SHADE_THICKNESS = "captcha.shade.thickness";

    // 生成验证码内容类型
    public static final String CAPTCHA_TEXT_TYPE = "captcha.text.type";

    // 验证码长度
    public static final String CAPTCHA_TEXT_LENGTH = "captcha.text.length";

    // 验证码字体名字
    public static final String CAPTCHA_TEXT_FONT_NAMES = "captcha.text.font.names";

    // 验证码字体大小
    public static final String CAPTCHA_TEXT_FONT_SIZE = "captcha.text.font.size";

    //验证码字体厚度
    public static final String CAPTCHA_TEXT_FONT_THICKNESS = "captcha.text.font.thickness";

    // 设置字体颜色
    public static final String CAPTCHA_TEXT_FONT_COLOR = "captcha.text.font.color";

    // 设置背景颜色
    public static final String CAPTCHA_BACKGROUND_COLOR = "captcha.background.color";

    // 添加扭曲效果
    public static final String CAPTCHA_IMAGE_SHEAR_ADD = "captcha.image.shear.add";

    // 设置图片类型
    public static final String CAPTCHA_IMAGE_TYPE = "captcha_image_type";

    // 图片宽度
    public static final String CAPTCHA_IMAGE_WIDTH = "captcha.image.width";

    // 图片高度
    public static final String CAPTCHA_IMAGE_HEIGHT = "captcha.image.height";
}
