package com.captcha.exception;


/**
 * @Author: 随风飘的云
 * @Description: 验证码文本异常处理器
 * @Date: 2022/2/24 10:39
 * @Modified By:
 */
public class TextException extends RuntimeException{

    private static final long serialVersionUID = 6937416954897707221L;

    public TextException (String message, Throwable cause) {
        super("The Text generation has some trouble and you can see the reason: " + message +
                ", which is caused by " + cause);
    }

    public TextException (String message) {
        super("The Image generation has some trouble and you can see the reason: " + message);
    }
}
