package com.captcha.exception;

/**
 * @Author: 随风飘的云
 * @Description: 配置类异常处理器
 * @Date: 2022/2/24 20:29
 * @Modified By:
 */
public class ConfigException extends RuntimeException{

    private static final long serialVersionUID = 6937416954897707291L;

    public ConfigException(String paramName, String paramValue, Throwable cause){
        super(
              "Your Configuration is illegal." + "The paramName is " + paramName + " and the paramValue is " + paramValue
              + ", which is caused by " + cause);
    }

    public ConfigException(String paramName, String paramValue, String message) {
        super(
                "Your Configuration is illegal." + "The paramName is " + paramName + " and the paramValue is " + paramValue
                        + ", You can view the detailed error reason:  " + message);
    }

}
