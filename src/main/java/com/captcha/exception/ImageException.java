package com.captcha.exception;

/**
 * @Author: 随风飘的云
 * @Description: 验证码图片生成异常处理器
 * @Date: 2022/2/24 10:40
 * @Modified By:
 */
public class ImageException extends RuntimeException{

    private static final long serialVersionUID = 6937416954897707201L;

    public ImageException(String message, Throwable cause) {
        super("The Image generation has some trouble and you can see the reason " + message +
                ", which is caused by " + cause);
    }

    public ImageException(String message) {
        super("The Image generation has some trouble and you can see the reason " + message);
    }
}
