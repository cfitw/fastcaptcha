# 前言
 首先这个项目是完全基于java底层开发的，没有经过任何的框架渲染，目前项目中集成了junit测试框架，主要使用`@Test`注解。
 **注意： 建议使用最新版的junit测试框架**

```java
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <scope>test</scope>
        </dependency>
```

# 示例：
## 1、png格式验证码

```java
@Test
    public void getPngCaptcha() throws Exception{
        PngCaptcha captcha = new PngCaptcha();
        Properties properties = new Properties();
        Config config = new Config(properties);
        captcha.setConfig(config);
        String text = captcha.getText();
        System.out.println(text);
        BufferedImage image = captcha.RenderImage(text);
        captcha.write(new FileOutputStream(new File("路径+验证码命名")), image);
    }
```
**效果：**
![在这里插入图片描述](https://img-blog.csdnimg.cn/c015aca230e04eaca6e898d44b3ef05e.png)

## 2、 gif格式验证码：

```java
@Test
    public void getGifCaptcha() throws Exception {
        Properties properties = new Properties();
        GifCaptcha captcha = new GifCaptcha();
        Config config = new Config(properties);
        captcha.setConfig(config);
        String text = captcha.getText();
        System.out.println(text);
        captcha.createGif(text);
        captcha.write(new FileOutputStream(new File("路径+验证码命名")), captcha.getImageBytes());
    }
```
**效果：**

![在这里插入图片描述](https://img-blog.csdnimg.cn/d2d0df5cb7b94a95a1a6fb7fe0b531f1.gif)
# 内置功能
## 1、任意切换图片大小
两行配置代码可以实现改变验证码图片大小：
```java
 properties.setProperty(CaptchaConstant.CAPTCHA_IMAGE_WIDTH, "300");
 properties.setProperty(CaptchaConstant.CAPTCHA_IMAGE_HEIGHT, "100");
```
效果：
![在这里插入图片描述](https://img-blog.csdnimg.cn/4b5259f363864d76abefc437e2ad3bfc.png)
## 2、切换字体需求（三种方法）
第一种：代码内置实现了字体配置（默认字体大小30）具体效果如上所示
第二种：自行添加内置字体（**字体来源均已在下文提及**）
 
设置字体：
```java
properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_FONT_NAMES, "Algerian,AlphabetSoup Tilt BT");
```
修改字体大小：

```java
properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_FONT_SIZE, "36");
```

png格式效果：

![在这里插入图片描述](https://img-blog.csdnimg.cn/c915736e8c024468960cf10201a21448.png)

gif格式效果：

![在这里插入图片描述](https://img-blog.csdnimg.cn/0aa5d56cfa58415a8127e27f0c6a4aad.gif)

第三种：将准备好的字体放在项目的根目录下的`resource`目录下

![在这里插入图片描述](https://img-blog.csdnimg.cn/49a110e6f14b44a1ae3650a4a1827d93.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5reh5aKoQO-9nuaXoOeXlQ==,size_20,color_FFFFFF,t_70,g_se,x_16)
 

```java
@Test
    public void getGifCaptcha() throws Exception {
        Properties properties = new Properties();
        GifCaptcha captcha = new GifCaptcha();
        Config config = new Config(properties);
        captcha.setConfig(config);
        String text = captcha.getText();
        // 注意这行代码
        captcha.setTextFont("CASTELAR.ttf", 36f);
        captcha.createGif(text);
        captcha.write(new FileOutputStream(new File("路径+验证码命名")), captcha.getImageBytes());
    }
```
效果：

![在这里插入图片描述](https://img-blog.csdnimg.cn/dc66df6852144a1b83018f656031cabc.gif)

## 3、干扰线，干扰圆
修改干扰线（默认干扰线数量为1）

```java
 		 // 设置干扰线数量（取消则设置数量为0）
        properties.setProperty(CaptchaConstant.CAPTCHA_SHADE_NUMBER, "0");
        // 设置干扰线厚度（厚度默认为1）
        properties.setProperty(CaptchaConstant.CAPTCHA_SHADE_THICKNESS, "5");
        // 设置干扰圆数量（取消则设置数量为0）
        properties.setProperty(CaptchaConstant.CAPTCHA_NOISE_NUMBER, "0");
```
取消干扰线干扰圆效果：

![在这里插入图片描述](https://img-blog.csdnimg.cn/19c0f24add004484a6c973c2fec4c980.png)

设置干扰线干扰圆效果（效果自行设置）：

```java
		// 设置干扰线数量（取消则设置数量为0）
        properties.setProperty(CaptchaConstant.CAPTCHA_SHADE_NUMBER, "3");
        // 设置干扰线厚度（厚度默认为1）
        properties.setProperty(CaptchaConstant.CAPTCHA_SHADE_THICKNESS, "5");
        // 设置干扰圆数量（取消则设置数量为0）
        properties.setProperty(CaptchaConstant.CAPTCHA_NOISE_NUMBER, "5");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/a9b9f52c24e3466da9ab4aa13a3a7855.png)

## 4、扭曲效果
**不添加扭曲效果：**

```java
 // 添加扭曲效果（默认true）  
 properties.setProperty(CaptchaConstant.CAPTCHA_IMAGE_SHEAR_ADD, "no");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/bd6f8b2ad5a04fcb9b204005f1b46de0.png)

**添加扭曲效果：**

```java
      // 添加扭曲效果（默认true）
        properties.setProperty(CaptchaConstant.CAPTCHA_IMAGE_SHEAR_ADD, "yes");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/f198c28860e640c5bc203350ee758b63.png)

## 5、设置验证码内容
**设置内容长度**：（默认为4位）

```java
      // 设置长度（默认四位）
        properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_LENGTH, "6");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/2247de834ab24294a1ef14aeda4c66a4.png)

设置内容格式：（内置10种内容格式）

```java
	// 数字
    NUMBER(1, "NUMBER"),
    // 中文数字
    NUMBER_ZH(2, "NUMBER_ZH"),
    // 中文
    CHINESE(3, "CHINESE"),
    // 点击中文成语
    CHINESE_IDIOM(4, "CHINESE_IDIOM"),
    // 小写英文
    CHAR(5, "CHAR"),
    // 大写
    UPPER_CHAR(6, "UPPER_CHAR"),
    // 英文和中文混合
    STRING(7, "STRING"),
    //数字和大小写字符验证码
    UPPER_STRING(8, "UPPER_STRING"),
    // 算术
    ARITHMETIC(9, "ARITHMETIC"),
    // 中文算术
    ARITHMETIC_ZH(10, "ARITHMETIC_ZH");
```
**示例：**

1、数字

```java
properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_TYPE, "NUMBER");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/4ac427d3a73540508a268f4541db4c6a.gif)

2、中文数字

```java
    // 设置长度（默认四位）
        properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_LENGTH, "6");
        properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_TYPE, "NUMBER_ZH");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/77773cedc1494467b36111376ab54d48.png)

3、随机中文
 
```java
properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_TYPE, "CHINESE");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/31ff48a1db32498d9f2bf355c56011f4.gif)

4、中文成语

```java
@Test
    public void getGifCaptcha() throws Exception {
        Properties properties = new Properties();
        GifCaptcha captcha = new GifCaptcha();
        // 注意这行代码
        properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_TYPE, "CHINESE_IDIOM");
        Config config = new Config(properties);
        captcha.setConfig(config);
        String text = captcha.getText();
        // 注意这行代码
        captcha.createGif(text.split(",")[1]);
        captcha.write(new FileOutputStream(new File("路径+验证码命名")), captcha.getImageBytes());
    }
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/774e04047c154fa782186dfe05c3b0c0.gif)

6、算术

```java
@Test
    public void getGifCaptcha() throws Exception {
        Properties properties = new Properties();
        GifCaptcha captcha = new GifCaptcha();
        // 这行
        properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_TYPE, "ARITHMETIC");
        Config config = new Config(properties);
        captcha.setConfig(config);
        String text = captcha.getText();
        System.out.println(text);
        // 这行
        int temp = text.indexOf("?");
        // 这行
        captcha.createGif(text.substring(0, temp));
        captcha.write(new FileOutputStream(new File("路径+验证码命名")), captcha.getImageBytes());
    }
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/0a2151243eae479285f173021600533b.gif)

7、中文算术

```java
properties.setProperty(CaptchaConstant.CAPTCHA_TEXT_TYPE, "ARITHMETIC_ZH");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/59420c1d3f7147d9af93ea3fbb65816e.gif)

## 6、设置边框
**设置无边框：**
```java
 properties.setProperty(CaptchaConstant.CAPTCHA_BORDER, "no");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/1c52238e6d6a44bc939e5f9dac855313.png)

**设置边框厚度及颜色：**
```java
	properties.setProperty(CaptchaConstant.CAPTCHA_BORDER_THICKNESS, "5");
        properties.setProperty(CaptchaConstant.CAPTCHA_BORDER_COLOR, "0,0,0");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/54ceb8141e72473a8e1abe7e55365bfe.png)

## 7、设置图片背景颜色
 

```java
properties.setProperty(CaptchaConstant.CAPTCHA_BACKGROUND_COLOR, "0,0,0");
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/a263838e62f745c1bc8aa29cbf9ef967.png)

# 致谢
这个框架花费了我不少的心思，同时也参考了一些**开源项目**的**解决方案**，**开源不易，感谢分享**

 - 感谢[**Kaptcha项目**](https://mvnrepository.com/artifact/com.github.penggle/kaptcha)提供需求配置方法的灵感
 - 感谢[**hutool工具类库开发者**](https://www.hutool.cn/)提供的底层工具支持
 
# 项目概况
 - 已经对项目的基本逻辑进行了测试（尚未深入地进行测试）
 - 项目内置了Base64编码工具类，可以实现将验证码图片进行编码
 - 暂时未完善项目的IO流，没有集成前后端数据传输交互
 - 没有完善简化操作验证码流程
 - 后续会重构底层，添加滑动验证码和行为点击验证码

# 最后
项目的所有[**源码**](https://gitee.com/cfitw/fastcaptcha)均已上传到码云，有需要者自行下载使用。

# **免责声明：**
项目开发所使用的字体均来源于所使用电脑的windows10系统提供的内置字体，使用者可自行查看 **`C:\Windows\Fonts`**目录下的字体，或者是在网上下载喜欢的字体去更改项目内置的字体，后续如有发生任何字体侵权行为，与项目开发者无关，特此声明！

